/**
 * 
 */
package org.graphql_forum_sample.client;

import java.io.IOException;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import org.forum.client.Shop;
import org.forum.client.util.GraphQLRequest;
import org.forum.client.util.QueryRootExecutor;
import org.glassfish.jersey.client.ClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.graphql_java_generator.exception.GraphQLRequestExecutionException;
import com.graphql_java_generator.exception.GraphQLRequestPreparationException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

public class GraphQLClient {

	/** The logger for this class */
	static protected Logger logger = LoggerFactory.getLogger(GraphQLClient.class);

	QueryRootExecutor queryExecutor;

	// Partial requests
	GraphQLRequest shopRequest;

	public GraphQLClient() throws GraphQLRequestPreparationException {

		// Creation of the query executor, for this GraphQL endpoint
		logger.info("Connecting to GraphQL endpoint");
//		queryExecutor = new QueryRootExecutor("https://storilabs.myshopify.com/admin/api/2021-01/graphql.json");
		ClientConfig config = new ClientConfig();
		ClientRequestFilter tokenHeadersAdder = new ClientRequestFilter()
		{
			public void filter(ClientRequestContext ctx) throws IOException
			{
				final Map<String, List<Object>> headers = ctx.getHeaders();
				headers.put("X-Shopify-Access-Token", Collections.singletonList("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"));
			}
		};
		Client client = ClientBuilder.newClient(config);
		client.register(tokenHeadersAdder);
		ObjectMapper o = new ObjectMapper();
		o.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		o.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
		o.disable(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE);


		queryExecutor = new QueryRootExecutor("https://storilabs.myshopify.com/admin/api/2021-01/graphql.json",client,o);

		shopRequest = queryExecutor
				.getShopGraphQLRequest("{ name\n" +
						"    primaryDomain {\n" +
						"      url\n" +
						"      sslEnabled\n" +
						"      host\n" +
						"    }\n" +
						"    features {\n" +
						"      eligibleForSubscriptions\n" +
						"      sellsSubscriptions\n" +
						"      eligibleForSubscriptionMigration\n" +
						"    }}"
					);
	}

	public void execPartialRequests() throws GraphQLRequestExecutionException {
		// Let's get, then display, all available boards
		Shop shop = queryExecutor.shop(shopRequest);
		logger.trace("Shop read: {}", shop);
	}


}
